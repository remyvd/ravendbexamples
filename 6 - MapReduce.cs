﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Client.Indexes;
using System.Linq;
using System.Collections.Generic;

namespace RavenDBExamples
{
    [TestClass]
    public class MapReduce : RavenTest
    {
        [TestMethod]
        public void UsingStaticIndexesWeCanDoAggregations()
        {
            #region Saving 5 of one, 3 of another and 2 of another
            using (var session = NewSession())
            {
                for (int i = 0; i < 5; i++)
                {
                    session.Store(new TodoList()
                    {
                        Items = new List<TodoItem>()
                        {
                            new TodoItem() { Category = "CategoryOne" }
                        }
                    });
                }

                for (int i = 0; i < 3; i++)
                {
                    session.Store(new TodoList()
                    {
                        Items = new List<TodoItem>()
                        {
                            new TodoItem() { Category = "CategoryTwo" }
                        }
                    });
                }

                for (int i = 0; i < 2; i++)
                {
                    session.Store(new TodoList()
                    {
                        Items = new List<TodoItem>()
                        {
                            new TodoItem() { Category = "CategoryThree" }
                        }
                    });
                }

                session.SaveChanges();
            }
            #endregion

            WaitForIndexing();

            using (var session = NewSession())
            {
                // Precalculated, so fast! No caching needed...
                var items = session.Query<CategoryCountItem, TodoList_CountCategories>()
                    .ToArray();

                Assert.AreNotEqual(0, items.Count());
                Assert.AreEqual(5, items.Where(x => x.Category == "CategoryOne").First().Count);
                Assert.AreEqual(3, items.Where(x => x.Category == "CategoryTwo").First().Count);
                Assert.AreEqual(2, items.Where(x => x.Category == "CategoryThree").First().Count);
            }
        }

        public class CategoryCountItem
        {
            public string Category { get; set; }
            public int Count { get; set; }
        }

        public class TodoList_CountCategories : AbstractIndexCreationTask<TodoList, CategoryCountItem>
        {
            public TodoList_CountCategories()
            {
                Map = docs => from doc in docs
                              from item in doc.Items
                              select new
                              {
                                  item.Category,
                                  Count = 1
                              };

                Reduce = results => from result in results
                                    group result by result.Category into g
                                    select new
                                    {
                                        Category = g.Key,
                                        Count = g.Sum(x => x.Count)
                                    };
            }
        }
    }
}

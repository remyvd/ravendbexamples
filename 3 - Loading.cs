﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RavenDBExamples
{
    [TestClass]
    public class Loading : RavenTest
    {
        [TestMethod]
        public void LookHowEasyItIsToLoadItemsOutById()
        {
            var id = string.Empty;

            #region Creating and saving the user
            using (var s = NewSession())
            {
                var user = new User()
                {
                    DisplayName = "Rémy van Duijkeren",
                    EmailAddress = "remy.van.duijkeren@dynamichands.nl",
                    Name = "remyvd"
                };

                s.Store(user);
                s.SaveChanges();

                id = user.Id;
            }
            #endregion

            using (var session = NewSession())
            {
                var user = session.Load<User>(id);

                Assert.IsNotNull(user);
            }
        }

        [TestMethod]
        public void LoadRelatedEntities()
        {
            var userId = string.Empty;
            var listId = string.Empty;

            #region Creating and saving the user and a todolist
            using (var session = NewSession())
            {
                var user = new User()
                {
                    DisplayName = "Rémy van Duijkeren",
                    EmailAddress = "remy.van.duijkeren@dynamichands.nl",
                    Name = "remyvd"
                };

                session.Store(user);
                userId = user.Id;

                var todolist = new TodoList()
                {
                    DeadLine = new DateTime(2013, 4, 12),
                    Name = "Finish RavenDB Presentation",
                    UserId = userId,
                    Items = new List<TodoItem>()
                    {
                        new TodoItem() { Title = "First todo item!" }
                    }
                };

                session.Store(todolist);
                listId = todolist.Id;

                session.SaveChanges();
            }
            #endregion

            using (var session = NewSession())
            {
                var todolist = session.Load<TodoList>(listId);
                
                // this will require querying the server!
                var user = session.Load<User>(todolist.UserId);

                Assert.IsNotNull(todolist);
                Assert.IsNotNull(user);
            }
        }

        [TestMethod]
        public void WeHaveJoinsKindOf()
        {
            var userId = string.Empty;
            var listId = string.Empty;

            #region Creating and saving the user and a todolist
            using (var session = NewSession())
            {
                var user = new User()
                {
                    DisplayName = "Rémy van Duijkeren",
                    EmailAddress = "remy.van.duijkeren@dynamichands.nl",
                    Name = "remyvd"
                };

                session.Store(user);
                userId = user.Id;

                var todolist = new TodoList()
                {
                    DeadLine = new DateTime(2013, 4, 12),
                    Name = "Finish RavenDB Presentation",
                    UserId = userId,
                    Items = new List<TodoItem>()
                    {
                        new TodoItem() { Title = "First todo item!" }
                    }
                };

                session.Store(todolist);
                listId = todolist.Id;

                session.SaveChanges();
            }
            #endregion

            using (var session = NewSession())
            {
                var todolist = session.Include<TodoList>(l => l.UserId)
                    .Load(listId);

                // this will not require querying the server!
                var user = session.Load<User>(todolist.UserId);

                Assert.IsNotNull(todolist);
                Assert.IsNotNull(user);
            }
        }
    }
}

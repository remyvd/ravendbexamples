﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Client;
using Raven.Client.Embedded;
using Raven.Client.Indexes;
using System.Threading;
using Raven.Client.Document;

namespace RavenDBExamples
{
    [TestClass]
    public class RavenTest
    {
        private IDocumentStore _store;

        [TestInitialize]
        public void Initialize()
        {
            _store = new EmbeddableDocumentStore()
            //_store = new DocumentStore() // use when RavenDB is started as Console
            {
                //Url = "http://localhost:8080", // use when RavenDB is started as Console
                //DefaultDatabase = "Examples" // use when RavenDB is started as Console
                //DataDirectory = "Data", // use when RavenDB is run as Embedded
                RunInMemory = true // use when RavenDB is run as Embedded
            };
            
            _store.Initialize();

            IndexCreation.CreateIndexes(this.GetType().Assembly, _store);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _store.Dispose();
        }

        public IDocumentSession NewSession()
        {
            return _store.OpenSession();
        }

        public void WaitForIndexing()
        {
            while (_store.DatabaseCommands.GetStatistics().StaleIndexes.Length > 0)
            {
                Thread.Sleep(200);
            }
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RavenDBExamples
{
    [TestClass]
    public class Storing : RavenTest
    {
        [TestMethod]
        public void WatchMeJustSaveThisPocoAndMarvelAtTheSimplicityOfItAll()
        {
            using (var session = NewSession())
            {
                // So we can create a user
                var user = new User()
                {
                    DisplayName = "Rémy van Duijkeren",
                    EmailAddress = "remy.van.duijkeren@dynamichands.nl",
                    Name = "remyvd"
                };

                Assert.IsNull(user.Id);
                session.Store(user);
                Assert.IsNotNull(user.Id);
                
                // Nothing has been sent to the server yet, also not for the id
                
                session.SaveChanges(); // Okay, now it has. Transaction :)!
            }            
        }

        [TestMethod]
        public void ThatMeansSerializingEntireDocuments()
        {
            using (var session = NewSession())
            {
                var todolist = new TodoList()
                {
                    DeadLine = new DateTime(2013, 4, 12),
                    Name = "Finish RavenDB Presentation",
                    Items = new List<TodoItem>()
                    {
                        new TodoItem()
                        {
                            Category = "Presentation",
                            IsComplete = false,
                            Title = "PPT",
                            Description = "Create a powerpoint"
                        },
                        new TodoItem()
                        {
                            Category = "Presentation",
                            IsComplete = false,
                            Title = "Make it fun",
                            Description = "Awkwardly inject humor into slides"
                        },
                        new TodoItem()
                        {
                            Category = "Presentation",
                            IsComplete = false,
                            Title = "Tests",
                            Description = "Create a lad of tests for demonstration"
                        }
                    }
                };

                // Serialize ENTIRE object into one giant JSON blob (=document)!!!
                session.Store(todolist);
                session.SaveChanges();
            }
        }

        [TestMethod]
        public void NoteTheCompleteAndUtterLackOfRelations()
        {
            User user;

            #region Creating and saving the user
            using (var s = NewSession())
            {
                user = new User()
                {
                    DisplayName = "Rémy van Duijkeren",
                    EmailAddress = "remy.van.duijkeren@dynamichands.nl",
                    Name = "remyvd"
                };

                s.Store(user);
                s.SaveChanges();
            }
            #endregion

            using (var session = NewSession())
            {
                #region Create todo list
                var todolist = new TodoList()
                {
                    DeadLine = new DateTime(2013, 4, 12),
                    Name = "Finish RavenDB Presentation",
                    Items = new List<TodoItem>()
                    {
                        new TodoItem()
                        {
                            Category = "Presentation",
                            IsComplete = false,
                            Title = "PPT",
                            Description = "Create a powerpoint"
                        },
                        new TodoItem()
                        {
                            Category = "Presentation",
                            IsComplete = false,
                            Title = "Make it fun",
                            Description = "Awkwardly inject humor into slides"
                        },
                        new TodoItem()
                        {
                            Category = "Presentation",
                            IsComplete = false,
                            Title = "Tests",
                            Description = "Create a lad of tests for demonstration"
                        }
                    }
                };
                #endregion

                // We only store an id to the user. As far as RavenDB is 
                // concerned it is JUST ANOTHER PROPERTY. Remember that!
                todolist.UserId = user.Id;
                
                session.Store(todolist);
                session.SaveChanges();
            }
        }
    }
}

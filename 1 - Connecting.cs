﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Embedded;

namespace RavenDBExamples
{
    [TestClass]
    public class Connecting
    {
        [TestMethod, Ignore]
        public void LookAtMeICanConnectToHttpServer()
        {
            // In Application start-up
            IDocumentStore db = new DocumentStore() { Url = "http://localhost:8080" };
            db.Initialize();

            // Per Unit of Work
            using (var session = db.OpenSession())
            {
                // Do work...
            }
        }

        [TestMethod, Ignore]
        public void AndICanAlsoRunInProcess()
        {
            // In Application start-up
            IDocumentStore db = new EmbeddableDocumentStore() { DataDirectory = "Data" };
            db.Initialize();

            // Per Unit of Work
            using (var session = db.OpenSession())
            {
                // Do work...
            }
        }

        [TestMethod, Ignore]
        public void AndEntirelyInMemoryForUberFastTesting()
        {
            // In Application start-up
            IDocumentStore db = new EmbeddableDocumentStore() { RunInMemory = true };
            db.Initialize();

            // Per Unit of Work
            using (var session = db.OpenSession())
            {
                // Do work...
            }
        }
    }
}

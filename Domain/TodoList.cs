using System;
using System.Collections.Generic;
using System.Text;

namespace RavenDBExamples
{
    public class TodoList
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public DateTime DeadLine { get; set; }
        public string Name { get; set; }
        public List<TodoItem> Items { get; set; }
    }
}

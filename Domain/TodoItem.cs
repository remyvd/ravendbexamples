using System;
using System.Collections.Generic;
using System.Text;

namespace RavenDBExamples
{
    public class TodoItem
    {
        public string Category { get; set; }
        public bool IsComplete {get; set;}
        public string Title {get; set;}
        public string Description {get; set;}
    }
}

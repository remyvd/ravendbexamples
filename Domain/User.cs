using System;
using System.Collections.Generic;
using System.Text;

namespace RavenDBExamples
{
    public class User
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string Name { get; set; }
    }
}

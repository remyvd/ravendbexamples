﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RavenDBExamples
{
    [TestClass]
    public class Modifying : RavenTest
    {
        [TestMethod]
        public void IfLoadAnItemAndModifyItChangesAreStored()
        {
            var id = string.Empty;

            #region Creating and saving the user
            using (var s = NewSession())
            {
                var user = new User()
                {
                    DisplayName = "Rémy van Duijkeren",
                    EmailAddress = "remy.van.duijkeren@dynamichands.nl",
                    Name = "remyvd"
                };

                s.Store(user);
                s.SaveChanges();

                id = user.Id;
            }
            #endregion

            using (var session = NewSession())
            {
                var user = session.Load<User>(id);
                user.DisplayName = "Alleen op de wereld";
                session.SaveChanges();
            }

            using (var session = NewSession())
            {
                var user = session.Load<User>(id);
                Assert.AreEqual("Alleen op de wereld", user.DisplayName);
            }
        }

        [TestMethod]
        public void IfIModifySeveralItemsTheWholeLotGetStoredAtomically()
        {
            var userId = string.Empty;
            var listId = string.Empty;

            #region Creating and saving the user and a todolist
            using (var session = NewSession())
            {
                var user = new User()
                {
                    DisplayName = "Rémy van Duijkeren",
                    EmailAddress = "remy.van.duijkeren@dynamichands.nl",
                    Name = "remyvd"
                };

                session.Store(user);
                userId = user.Id;

                var todolist = new TodoList()
                {
                    DeadLine = new DateTime(2013, 4, 12),
                    Name = "Finish RavenDB Presentation",
                    UserId = userId,
                    Items = new List<TodoItem>()
                    {
                        new TodoItem() { Title = "First todo item!" }
                    }
                };

                session.Store(todolist);
                listId = todolist.Id;

                session.SaveChanges();
            }
            #endregion

            using (var session = NewSession())
            {
                var user = session.Load<User>(userId);
                var list = session.Load<TodoList>(listId);

                user.DisplayName = "Alleen op de wereld";
                list.Items.Add(new TodoItem() { Title = "Another todo item!"} );

                // This saves ALL changes atomically. YES! A unit of work with a single request.
                session.SaveChanges();
            }

            using (var session = NewSession())
            {
                var user = session.Load<User>(userId);
                var list = session.Load<TodoList>(listId);

                Assert.AreEqual("Alleen op de wereld", user.DisplayName);
                Assert.AreEqual(2, list.Items.Count);
            }
        }
    }    
}

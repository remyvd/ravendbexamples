﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Raven.Client.Linq;
using System.Linq;
using Raven.Client.Indexes;

namespace RavenDBExamples
{
    [TestClass]
    public class Querying : RavenTest
    {
        [TestMethod]
        public void LookAtMeICanQueryDocumentsWithoutAnIndex()
        {
            #region Saving 5 list for one user, and 3 lists for another
            using (var session = NewSession())
            {
                for (int i = 0; i < 5; i++)
                {
                    session.Store(new TodoList()
                    {
                        UserId = "users/1",
                        Name = "Dinner" + i
                    });
                }

                for (int i = 0; i < 3; i++)
                {
                    session.Store(new TodoList()
                    {
                        UserId = "users/2",
                        Name = "Lunch" + i
                    });
                }

                session.SaveChanges();
            }
            #endregion

            using (var session = NewSession())
            {
                var listForUser1 = (from list in session.Query<TodoList>()
                                    where list.Name.StartsWith("Lunch")
                                    select list)
                                    .ToArray();

                Assert.AreEqual(3, listForUser1.Length);
            }
        }

        [TestMethod]
        public void YesThatIncludesCollectionsToo()
        {
            #region Saving 5 list, 3 with RavenDB as category
            using (var session = NewSession())
            {
                for (int i = 0; i < 3; i++)
                {
                    session.Store(new TodoList()
                    {
                        UserId = "users/1",
                        Items = new List<TodoItem>()
                        {
                            new TodoItem()
                            {
                                Category = "RavenDB",
                                IsComplete = false,
                                Title = "No thinking about mapping!",
                            },
                        }
                    });
                }

                for (int i = 0; i < 2; i++)
                {
                    session.Store(new TodoList()
                    {
                        UserId = "users/1",
                        Items = new List<TodoItem>()
                        {
                            new TodoItem()
                            {
                                Category = "SQL Server",
                                IsComplete = false,
                                Title = "Why use it, if we got RavenDB?",
                            },
                        }
                    });
                }

                session.SaveChanges();
            }
            #endregion

            using (var session = NewSession())
            {
                var listWithRavenDBItems = (from list in session.Query<TodoList>()
                                            where list.Items.Any(x => x.Category == "RavenDB")
                                            select list)
                                            .ToArray();

                Assert.AreEqual(3, listWithRavenDBItems.Length);
            }
            
        }

        [TestMethod]
        public void ButWeCanUseHomeMadeIndexesIfWeWant()
        {
            #region Saving 5 list for one user, and 3 lists for another
            using (var session = NewSession())
            {
                for (int i = 0; i < 5; i++)
                {
                    session.Store(new TodoList()
                    {
                        UserId = "users/1"
                    });
                }

                for (int i = 0; i < 3; i++)
                {
                    session.Store(new TodoList()
                    {
                        UserId = "users/2"
                    });
                }

                session.SaveChanges();
            }
            #endregion

            WaitForIndexing();

            using (var session = NewSession())
            {
                var listForUser1 = (from list in session.Query<TodoList, TodoList_ByUser>()
                                    where list.UserId == "users/1"
                                    select list)
                                    .ToArray();
                
                Assert.AreEqual(5, listForUser1.Length);
            }
        }

        public class TodoList_ByUser : AbstractIndexCreationTask<TodoList>
        {
            public TodoList_ByUser()
            {
                Map = docs => from doc in docs
                              select new { doc.UserId };
            }
        }
    }
}

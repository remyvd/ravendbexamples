﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Client.Indexes;
using System.Linq;
using System.Collections.Generic;

namespace RavenDBExamples
{
    [TestClass]
    public class LiveProjections : RavenTest
    {
        // SQL VERSION
        //-------------------------------------
        // SELECT  tl.UserId,
        //         tl.Name,
        //         [TaskCount] = (SELECT COUNT(*)) FROM TodoListItems tli WHERE tli.TaskId = tl.Id),
        // FROM    TodoList tl
        //         LEFT JOIN Users u ON tl.UserId = uint.UserId                

        [TestMethod]
        public void DontDenormalizeDataJustForQuering()
        {
            var userId = string.Empty;

            #region Save data
            using (var session = NewSession())
            {
                var user = new User()
                {
                    DisplayName = "Rémy van Duijkeren",
                    EmailAddress = "remy.van.duijkeren@dynamichands.nl",
                    Name = "remyvd"
                };

                session.Store(user);
                userId = user.Id;

                var todolist = new TodoList()
                {
                    DeadLine = new DateTime(2013, 4, 12),
                    Name = "Finish RavenDB Presentation",
                    UserId = userId,
                    Items = new List<TodoItem>()
                    {
                        new TodoItem() { Title = "First todo item!" },
                        new TodoItem() { Title = "Another todo item!" },
                        new TodoItem() { Title = "And another one!" }
                    }
                };

                session.Store(todolist);

                session.SaveChanges();
            }
            #endregion

            WaitForIndexing();

            using (var session = NewSession())
            {
                // Keep in mind that this is less scalable.
                var result = session.Query<TodoListView, TodoListViewIndex>()
                             .Where(x => x.UserId == userId)
                             .First();

                Assert.AreEqual("Rémy van Duijkeren", result.UserDisplayName);
                Assert.AreEqual(3, result.TaskCount);
            }
        }

        public class TodoListView
        {
            public string UserId { get; set; }
            public string Name { get; set; }
            public int TaskCount { get; set; }
            public string UserDisplayName { get; set; }
        }

        public class TodoListViewIndex : AbstractIndexCreationTask<TodoList>
        {
            public TodoListViewIndex()
            {
                Map = docs => from doc in docs
                              select new
                              {
                                  UserId = doc.UserId
                              };

                // On the server, please transform the results
                TransformResults = (database, results) =>
                    from result in results
                    let user = database.Load<User>(result.UserId)
                    select new
                    {
                        UserId = result.UserId,
                        Name = result.Items.Count,
                        TaskCount = result.Items.Count,
                        UserDisplayName = user.DisplayName
                    };
            }
        }
    }
}
